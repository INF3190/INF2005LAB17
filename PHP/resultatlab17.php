<?php
// Solution lab17 INF2005 @ UQAM 
require 'head.php';
// cette solution illustre plusieurs possibilités de traiter les données d'un formulaires
// L'accent n'est pas mis sur la validation des données mais sur l'accès et manipulaition
// Les classes bootstrap 3 sont utilisées pour l'affichage. 
// On illustre aussi comment ajouter son propre CSS pour ajuster l'affichage.
if(!isset($_REQUEST["soumettre"])){
    // isset veérifie si une variable est initialisée
   die("<span style='color:red;'>Erreur: Aucun formulaire soumis</span>"); 
}
$nom=$_REQUEST["nom"];
$prenom=$_REQUEST["prenom"];
$genre=$_REQUEST["genre"];
?>
<table class="table">
<caption class="text-center h3 text-primary">Identification</caption>
  <tr>
    <th>Prénom</th>
    <th>Nom</th>
    <th>Genre</th>
  </tr>
  <tr>
    <td><?=$prenom ?></td>
    <td><?=$nom ?></td>
    <td><?=$genre ?></td>
  </tr>
</table>

<?php 
$langues=$_REQUEST["langues"];
echo "<span class='h4 text-primary'>Langues: </span>";
echo "<ul class='list-inline' style='display:inline; list-style:none;'>";
$separator=",";//séparateurs des langues affichées
$nblangues = count($langues); // nombre langues sélectionnées
for($i=0;$i< $nblangues;$i++){
    $langue = $langues[$i];
    $langue = trim($langue);// trim() enlève les caractères blancs au début et à la fin
    if($i<$nblangues-1){// s'il y a encore une autre langue a afficher, on met un séparateur
        $langue = $langue.$separator;
    }
    echo "<li>".ucfirst($langue)."</li>";//ucfirst met le premier caractère en majuscule
}
echo "</ul>";
echo "<hr>";
echo "<h3 class='text-primary'>Informations université et ville</h3>";

if(isset($_REQUEST['universite'])){// issue de bouton radio
    echo "<span class='h5'>Université: ". strtoupper($_REQUEST['universite'])."</span><br/>";
    // strtoupper($chaine) mets $chaine en majuscule.
    // strtolower($chaine) mets %chaine  minuscule.
}
//echo json_encode($_REQUEST);

if(isset($_REQUEST['villes'])){
    echo "<span class='h5'>Villes sélectionnées :</span>";
    $villes= $_REQUEST['villes']; //un tebleau des checkboxes (cases à cocher)
    echo "<ul>";
    foreach ($villes as $ville){
        $ville= trim($ville); // trim enleve espace blanc autour de la chaine
        $ville= strtoupper($ville);
        echo "<li>$ville</li>";
    }
    echo "</ul>";
}
echo "<hr>";
echo "<h3 class='text-primary'>Informations Fichier</h3>";
if(isset($_FILES["fichier"])){
    // "fichier" est le nom du champ fichier dan le formulaire
    $infoFich = $_FILES["fichier"];
    $fileName = $infoFich['name']; // nom du fichier chargé par l,utilisateur
    $tmpFile=$infoFich['tmp_name'];// fichier temporaire après téléversement
    $saveDir="images";
    $savedFile="$saveDir"."/$fileName";
    echo "<dl class='dl-horizontal'>";
    echo "<dt>Nom: </dt>";
    echo "<dd>".$infoFich['name']."</dd>";
    //------
    echo "<dt>Type: </dt>";
    echo "<dd>".$infoFich['type']."</dd>";
    //------
    echo "<dt>Fichier temporaire: </dt>";
    echo "<dd>".$infoFich['tmp_name']."</dd>";
    //------
    echo "<dt>Emplace d'enregistrement: </dt>";
    echo "<dd>".$savedFile."</dd>";
    //------
    echo "<dt>Taille: </dt>";
    echo "<dd>".$infoFich['size']."</dd>";
    //------
    echo "<dt>Erreur: </dt>";
    echo "<dd>".$infoFich['error']."</dd>";
    
    echo "</dl>";
    //enregistrement du fichier
    if(!file_exists($saveDir)){
        //si le dossier n'existe pas
        mkdir("$saveDir")||die("<span style='color:red'>impossible de créer le dossier $saveDir</span>");
        // Au besoins donner les permission en écriture dans le dossier : chmod o+w NOM_DOSSIER
        // NOM_DOSSIER doit correspondre au non du dossier dans lequel on doit créer le sous dossier pour enregistrer
    }
    
    move_uploaded_file("$tmpFile","$savedFile")||die("<span style='color:red'>impossible de saugarder $fileName  dans $saveDir</span>");
    //move_uploaded_file($old,$new) enregistrer le fichier televersé d,un dossier à l'autre 
   //affichage image charge
   echo "<img src='".$savedFile."' class='img img-circle center-block'  style='width:200px;' alt='image téléversée'>";
}
echo "<hr>";
$commentaire=$_REQUEST["commentaire"];
echo "<div class='row'>"; // on veux utiliser les grilles bootstrap
echo "<div class='col-sm-2 h3 text-primary'>Commentaire: </div>";
echo "<div class='col-sm-10'>$commentaire</div>";
echo "</div>";
echo "<hr/>";
echo "<h3 class='text-primary'>Tableaux PHP et JSON des informations brutes du formulaire</h3>";
echo "<div class='row'>";
echo '<div class="col-sm-2 h3 text-primary"> $_REQUEST: </div>';
echo "<div class='col-sm-5'><pre>";
 print_r($_REQUEST);
 echo "</pre></div>";
 echo "<div class='col-sm-5'><pre>";
 echo json_encode($_REQUEST,JSON_UNESCAPED_UNICODE|JSON_FORCE_OBJECT);
 // l'option JSON_FORCE_OBJECT aide a convertir les tableaus non associatifs en json avec clés numériques
 // voir https://www.php.net/manual/en/json.constants.php
 echo "</pre></div>";
echo "</div>";
echo "<div class='row'>";
echo '<div class="col-sm-2 h3 text-primary">$_FILE: </div>';
echo "<div class='col-sm-5'><pre>";
print_r($_FILES);
echo "</pre></div>";
echo "<div class='col-sm-5'><pre>";
echo json_encode($_FILES,JSON_UNESCAPED_UNICODE|JSON_FORCE_OBJECT);
//l'option JSON_FORCE_OBJECT aide a convertir les tableaus non associatifs en json avec clés numériques
echo "</pre></div>";
echo "</div>";

require 'tail.php';